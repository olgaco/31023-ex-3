<?php
require "bootstrap.php";
use Chatter\Models\Message; 
use Chatter\Models\User;
use Chatter\Middleware\Logging; 

$app = new \Slim\App();
$app->add(new Logging()); //add the midleware (Logging) layer

//lines 8-16 is lesson 2
$app->get('/hello/{name}', function($request, $response,$args){
    return $response->write('Hello '.$args['name']);
});
$app->get('/customer/{number}', function($request, $response,$args){
    return $response->write('customer '.$args['number']. ' welcome to JCE');
});
$app->get('/customer/{cnumber}/product/{pnumber}', function($request, $response,$args){
    return $response->write('customer '.$args['cnumber']. ' product '.$args['pnumber']);
});

//READ MESSAGE - get the message information from database
$app->get('/messages', function($request, $response,$args){
    $_message = new Message();
    $messages = $_message->all();
    $payload = [];
    foreach($messages as $msg){
        $payload[$msg->id] = [
            'body'=>$msg->body,
            'user_id'=>$msg->user_id,
            'created_at'=>$msg->created_at
        ];
    }
    return $response->withStatus(200)->withJson($payload);
});
//CREATE MESSAGE - insert message to database
$app->post('/messages', function($request, $response,$args){
    $message = $request->getParsedBodyParam('message',''); //the body sheuzan
    $userid = $request->getParsedBodyParam('userid',''); //the body sheuzan
    $_message = new Message(); //create new message
    $_message->body = $message;
    $_message->user_id = $userid;
    $_message->save();
    if($_message->id){
        $payload = ['message'=>$_message->id];
        return $response->withStatus(201)->withJson($payload);
    } else {
        return $response->withStatus(400);
    }
});

//DELETE MESSAGE - delete message from database
$app->delete('/messages/{message_id}', function($request, $response,$args){
    $message = Message::find($args['message_id']); //find the message in database
    $message->delete(); //delete the message
    if($message->exists){ //check if the message already exit
        return $response->withStatus(400); //exist -> false error
    } else {
        return $response->withStatus(200); //message delete succesfully
    }
});

//PUT MESSAGE - update message from database
$app->put('/messages/{message_id}', function($request, $response,$args){
    $new_message = $request->getParsedBodyParam('message','');
    $_message = Message::find($args['message_id']);
    $_message->body = $new_message;
     if($_message->save()){
        $payload = ['message_id'=>$_message->id,"result"=>"The message has been updated successfuly"];
        return $response->withStatus(200)->withJson($payload);
     } else {
        return $response->withStatus(400);
     }
});

//BULK MESSAGE - insert a number of messages into the DB, with JSON input
$app->post('/messages/bulk', function($request, $response,$args){
    $payload = $request->getParsedBody(); //convert the JSON to array of
    Message::insert($payload); //the in the DB all what come from the JSON
    return $response->withStatus(201)->withJson($payload);
});

//READ USER - get the user information from database
$app->get('/users', function($request, $response,$args){
    $_user = new User(); //create new message
    $users = $_user->all();
    $payload = [];
    foreach($users as $usr){
        $payload[$usr->id] = [
            'username'=>$usr->username,
            'email'=>$usr->email
        ];
    }
    return $response->withStatus(200)->withJson($payload);
});

//CREATE USER - insert user to database
$app->post('/users', function($request, $response,$args){
    $user = $request->getParsedBodyParam('user',''); //the user sheuzan
    $mail = $request->getParsedBodyParam('mail',''); //the user sheuzan
    $_user = new user(); //create new user
    $_user->username = $user;
    $_user->email = $mail;
    $_user->save();
    $_user->file_put_contents($user, $mail);
    if($_user->id){
        $payload = ['user'=>$_user->id];
        return $response->withStatus(201)->withJson($payload);
    } else {
        return $response->withStatus(400);
    }
});

//DELETE USER - delete user from database
$app->delete('/users/{user_id}', function($request, $response,$args){
    $user = User::find($args['user_id']); //find the user in database
    $user->delete(); //delete the user
    if($user->exists){ //check if the user already exit
        return $response->withStatus(400); //exist -> false error
    } else {
        return $response->withStatus(200); //user delete succesfully
    }
});

//BULK USER POST- insert a number of messages into the DB, with JSON input
$app->post('/users/bulk', function($request, $response,$args){
    $payload = $request->getParsedBody(); //convert the JSON to array of
    User::insert($payload); //the in the DB all what come from the JSON
    return $response->withStatus(201)->withJson($payload);
});
//BULK USER DELETE - insert a number of messages into the DB, with JSON input
$app->delete('/users/bulk', function($request, $response,$args){
    $payload = $request->getParsedBody(); //convert the JSON to array of
    User::delete($payload); //the in the DB all what come from the JSON
    return $response->withStatus(201)->withJson($payload);
});

$app->run();